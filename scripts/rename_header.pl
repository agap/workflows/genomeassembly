#!/usr/bin/env perl
use Bio::SeqIO;

my $in_fasta = shift;
my $in_agp   = shift;
my $haplotype = shift;
my $out_fasta = shift;
my $out_agp   = shift;
my $out_txt   = shift;


my %chr = (
    "CM071913.1" => "chr1",
    "CM071914.1" => "chr2",
    "CM071915.1" => "chr3",
    "CM071916.1" => "chr4",
    "CM071917.1" => "chr5",
    "CM071918.1" => "chr6",
    "CM071919.1" => "chr7",
    "CM071920.1" => "chr8",
    "CM071921.1" => "chr9",
    "CM071922.1" => "chr10",
    "CM071923.1" => "chr11"
);


open(IN,$in_agp);
open(OUT,">$out_agp");
while(<IN>){
    chomp;
    if ($_ =~ /\#/){
        print OUT $_ ,"\n";
    }
    else {
        my @data = (split(/\t/,$_));
        my $id = shift(@data);
        $id = (split(/\_/,$id))[0];
        if ($chr{$id}){
            print OUT join("\t",$chr{$id}.".".$haplotype,@data),"\n";
        }
        else {
            print OUT $_,"\n";
        }
    }
}
close IN;
close OUT;
open(TXT,">$out_txt");
my $in = new Bio::SeqIO(
    -file => $in_fasta,
    -format => 'fasta'
);
my $out = new Bio::SeqIO(
    -file => ">$out_fasta",
    -format => 'fasta'
); 
while(my $seqobj = $in->next_seq){
    my $display_id = (split(/\_/,$seqobj->display_id()))[0];
    if ($chr{$display_id}) {
        my $new_id = $chr{$display_id}.".".$haplotype;
        $display_id = $seqobj->display_id($new_id);
        $out->write_seq($seqobj);
        print TXT join("\t",$new_id,$seqobj->length),"\n";
    }
    else { 
        $out->write_seq($seqobj);

    }
}
close TXT;
$in->close;
$out->close;
