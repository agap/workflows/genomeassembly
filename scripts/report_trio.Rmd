---
title: "Assembly Report"
author:
    -
date: "`r format(Sys.time(), '%d %B, %Y')`"
params:
   rmd: "report.Rmd"
output:
  html_document:
    highlight: tango
    number_sections: no
    theme: default
    toc: yes
    toc_depth: 3
    toc_float:
      collapsed: no
      smooth_scroll: yes
---

----

# `r snakemake@params[["sample"]]` 
* Sample : `r snakemake@params[["sample"]]`
* Hifiasm mode : `r snakemake@params[["mode"]]`

----

### Raw data QC
![](`r paste0(getwd(),"/",snakemake@input[["genomescope"]])`)

### K-mer profiles
| Hap 1 | Hap 2 |
|-------|-------|
| ![](`r paste0(getwd(),"/",snakemake@input[["kaplot_haplotype1"]])`) | ![](`r paste0(getwd(),"/",snakemake@input[["kaplot_haplotype2"]])`) |


### k-mer completeness (recovery rate)

```{r comment='', echo=FALSE}
cat(readLines(snakemake@input[["completeness"]]), sep = '\n')
```

#### Reference free QV estimation
```{r comment='', echo=FALSE}
cat(readLines(snakemake@input[["qv"]]), sep = '\n')
```


### Copy-number spectrum analysis
| Hap 1 | Hap 2 |
|-------|-------|
| ![](`r snakemake@input[["spectra_cn1"]]`) | ![](`r snakemake@input[["spectra_cn2"]]`) |

### Hap-mer blob plots

The blob plots provides a quick glance on the overall phasing across an assembly. Each dot (circle) represents a sequence, a contig or scaffold, with its size relative to the sequence length. Each sequence is colored by assembly. The x and y axis are the number of hap-mers found.

![](`r paste0(getwd(),"/",snakemake@input[["hapmers_blob"]])`)

### Phased block statistics and switch error rates

Blocks are visualized in the N* plots, ordered by its’ size along the block sum coverage. The blocks are colored according to the haplotype. The trio binned assemblies have small blocks of switches to the other haplotype, which indicates the small fraction of reads being mis-binned. (We expect this will become smaller when the error rate in the long reads go down.)

| Hap 1 | Hap 2 |
|-------|-------|
| ![](`r paste0(getwd(),"/",snakemake@input[["block_hap1"]])`) | ![](`r paste0(getwd(),"/",snakemake@input[["block_hap2"]])`) |


### Scaffolding using RagTag
```{r comment='', echo=FALSE}
data1 = read.table(snakemake@input[["coverage1"]], header = FALSE, sep = "\t",nrows=20)
knitr::kable(data1, booktabs = TRUE,col.names=c("rname","startpos","endpos","numreads","covbases","coverage","meandepth","meanbaseq","meanmapq"),caption = 'Read Depth haplotype1.')
```

```{r comment='', echo=FALSE}
data2 = read.table(snakemake@input[["coverage2"]], header = FALSE, sep = "\t",nrows=20)
knitr::kable(data2, booktabs = TRUE,col.names=c("rname","startpos","endpos","numreads","covbases","coverage","meandepth","meanbaseq","meanmapq"), caption = 'Read Depth haplotype2')
```
### Quast report

```{r comment='', echo=FALSE}
data2 = read.table(snakemake@input[["quast"]], header = TRUE, sep = "\t",nrows=20)
knitr::kable(data2, booktabs = TRUE,col.names=c("Assembly","hifiasm hap1","hifiasm hap2","purge_dup hap1","purge_dup hap2","ragtag hap1","ragtag hap2"), caption = 'Read Depth haplotype2')
```


### BUSCO
#### Haplotype 1
```{r comment='', echo=FALSE}
cat(readLines(snakemake@input[["busco_haplotype1"]]), sep = '\n')
```
#### Haplotype 2
```{r comment='', echo=FALSE}
cat(readLines(snakemake@input[["busco_haplotype2"]]), sep = '\n')
```

### BUSCO plot
![](`r paste0(getwd(),"/",snakemake@input[["busco_plot"]])`)