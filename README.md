# Snakemake workflow: Genome Assembly

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)
[![License: GNU](https://img.shields.io/badge/License-GNU-yellow.svg)](https://opensource.org/license/gpl-3-0)

**Table of Contents**
 
  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Overview of workflow](#overview-of-workflow)
  - [Procedure](#procedure)
  - [Output](#output)
  - [Citations](#citations)
 
## Objective

Genome assembly workflow using PacBio HiFi data.

## Dependencies

* cutadapt : removes adapter sequences from sequencing reads. (https://github.com/marcelm/cutadapt) 
* hifiasm : Hifiasm is a fast haplotype-resolved de novo assembler initially designed for PacBio HiFi reads. Its latest release could support the telomere-to-telomere assembly by utilizing ultralong Oxford Nanopore reads. (https://github.com/chhylp123/hifiasm)
* verkko : is a hybrid genome assembly pipeline developed for telomere-to-telomere assembly of PacBio HiFi or Oxford Nanopore Duplex and Oxford Nanopore simplex reads (https://github.com/marbl/verkko)
* yak : Yet another k-mer analyzer (https://github.com/lh3/yak)
* genomescope : Reference-free profiling of polyploid genomes (https://github.com/tbenavi1/genomescope2.0)
* jellyfish : A fast multi-threaded k-mer counter (https://github.com/gmarcais/Jellyfish)
* gfatools : Tools for manipulating sequence graphs in the GFA and rGFA formats (https://github.com/lh3/gfatools)
* busco : Assessing genomic data quality and beyond (https://busco.ezlab.org/)
* kat : The K-mer Analysis Toolkit (KAT) contains a number of tools that analyse and compare K-mer spectra. (https://github.com/TGAC/KAT)
* purge_dups : haplotypic duplication identification tool (https://github.com/dfguan/purge_dups)
* quast : Quality Assessment Tool for Genome Assemblies (https://quast.sourceforge.net/quast.html)
* merqury : k-mer based assembly evaluation (https://github.com/marbl/merqury)
* ragtag : Tools for fast and flexible genome assembly scaffolding and improvement (https://github.com/malonge/RagTag)
* markdown : R Markdown is a tool for creating reproducible and engaging reports, presentations, dashboards, and more with R, Python, and SQL. (https://rmarkdown.rstudio.com/)
* multiqc : Aggregate results from bioinformatics analyses across many samples into a single report (https://multiqc.info/)
* pbtk: PacBio BAM toolkit (https://github.com/PacificBiosciences/pbtk)
* yahs: YaHS: yet another Hi-C scaffolding tool (https://github.com/c-zhou/yahs)
* juicer_tools: (https://github.com/aidenlab/juicer/wiki/Juicer-Tools-Quick-Start) 
* pairtools: A simple and fast command-line framework to process sequencing data from a Hi-C experiment (https://pairtools.readthedocs.io/en/latest/index.html)
* tidk: is a toolkit to identify and visualise telomeric repeats for the Darwin Tree of Life genomes (https://github.com/tolkit/telomeric-identifier)

## Prerequisites

- Globally installed SLURM 18.08.7.1+
- Globally installed singularity 3.4.1+
- Installed Snakemake 6.0.0+

## Overview of workflow

<p align="center">
<img src="images/dag.png" width="50%">
</p>


## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
```bash
git clone https://gitlab.cirad.fr/umr_agap/workflows/genomeassembly.git
```


- Change directories into the cloned repository:

```bash
cd genomeassembly
```

- Convert PacBio bam to fastq.gz (if needed). See PacBio BAM toolkit (https://github.com/PacificBiosciences/pbtk)

```bash
wget https://depot.galaxyproject.org/singularity/pbtk%3A3.1.1--h9ee0642_0

# generates out.fastq.gz
singularity exec pbtk:3.1.1--h9ee0642_0 bam2fastq -j 4 -o out in.bam
 
```


- Edit the configuration file config.yaml

3 modes are available : 
  * default
  * trio
  * hic

The accession name must be unique 

```bash
# 
# input:
#  "accession":
#    "mode": "default|trio|hic" 
#    "raw_data": "path2fastq"
#    "maternal_raw_data": "path2mother" (required for trio mode)
#    "paternal_raw_data": "path2father" (required for trio mode)
#    "r1": "path2hic_r1"  (required for hic mode)
#    "r2": "path2hic_r2" (required for hic mode)
#    "ul": "path2ont" (optional)

input:
  "merlot_root":
    "mode": "trio"
    "raw_data": "raw_data/ERR10930364.fastq.gz"
    "maternal_raw_data": "cabernet_franc"
    "paternal_raw_data": "magdeleine_noire_des_charentes"
    "ul": ""
  "cabernet_franc":
    "mode": "default"
    "raw_data": "raw_data/ERR10930362.fastq.gz"
    "ul": ""
  "magdeleine_noire_des_charentes":
    "mode": "default"
    "raw_data": "raw_data/ERR10930361.fastq.gz"
    "ul": ""
  "merlot":
    "mode": "hic"
    "raw_data": "raw_data/ERR10930364.fastq.gz"
    "r1": "raw_data/X_R1.fastq.gz"
    "r2": "raw_data/X_R2.fastq.gz"
    "ul": "raw_data/ul.fastq.gz"

# number of threads 
threads: 4

# k-mers
kmer: 21

# Ploidy level
ploidy: 2

# Busco Lineage
busco_lineage: eudicots_odb10 

# Reference fasta
reference: "/storage/replicated/cirad/projects/DAAV/0_DONNEES_COMMUNES/REFERENCE_PN40024/reference_PN40024/PN40024_40X_REF_chloro_mito.fasta"

# Reference annotation 
gff: "/storage/replicated/cirad/projects/DAAV/0_DONNEES_COMMUNES/REFERENCE_PN40024/reference_PN40024/sequences.gff3"
```

- Edit the configuration file profile/config.yaml for SLURM integration
  * partition
  * mem_mb

## For Meso@LR

- Load module
```bash
module load snakemake/7.15.1-conda
module load singularity/3.5
```

- Print shell commands to validate your modification (mode dry-run)

```bash
sbatch snakemake dry
```

- Run workflow on Meso@LR

```bash
sbatch snakemake.sh run
```
     
- Unlock directory

```bash
sbatch snakemake.sh unlock
```
      
- Generate DAG file

```bash
sbatch snakemake.sh dag
```
             
## Output : 

All the output are prefixed with the accession name defined in the config.yaml file.

- Assembly
  - Haplotype 1 : results/*accession*/assembly/*accession*_hap1.fasta
  - Haplotype 2 : results/*accession*/assembly/*accession*_hap2.fasta

- Quality control
  - Report HTML : results/*accession*/report_*mode*.html
  - Quast : 
      * results/*accession*/quast/report.pdf
      * results/*accession*/quast/report.html
  - Busco : 
      * Haplotype1 : results/*accession*/busco/*accession*_hap1/short_summary.specific.*lineage*.*accession*_hap1.txt
      * Haplotype2 : results/*accession*/busco/*accession*_hap2/short_summary.specific.*lineage*.*accession*_hap2.txt

See example : 
  * [report_default](example/report_default.md)
  * [report_trio](example/report_trio.md)

## Citations

This workflow is based on a pre-existing workflow produced by Sukanya Denni and Ludovic Duvaux. 
https://forgemia.inra.fr/asm4pg/GenomAsm4pg