#!/usr/bin/env python
import pandas as pd
from pprint import pprint
import os,sys 

configfile:"config.yaml" 
reference=config["reference"]
lineage=config["busco_lineage"] 
input=config["input"]
assembler=config["assembler"]
haplotype = ["1","2"]
absolute_path = os.path.abspath("Snakemake")
basename = os.path.dirname(os.path.abspath("Snakemake"))

wildcard_constraints:
    sample =  "|".join(input.keys()) 


def get_mode(wildcards):
    mode = input[(wildcards.sample)]["mode"]
    return mode

def with_ont(wildcards):
    ul = input[(wildcards.sample)]["ul"]
    params = ""
    if ul != "":
        if assembler=="hifiasm":
            params = f"--ul " + ul
        else:
            params = f"--nano " + ul
    return params

def create_symlink(wildcards):
    mode = input[(wildcards.sample)]["mode"]
    file = []
    if mode == "trio": 
        maternal = input[wildcards.sample]["maternal_raw_data"]
        paternal = input[wildcards.sample]["paternal_raw_data"]
        maternal_path = f"results/meryl/"+maternal+".meryl"
        paternal_path = f"results/meryl/"+paternal+".meryl"
        file.append(maternal_path)
        file.append(paternal_path)
    return file
    

def create_symlink_hifiasm(wildcards):
    mode = input[(wildcards.sample)]["mode"]
    file = []
    if mode == "trio":   
        maternal = input[wildcards.sample]["maternal_raw_data"]
        paternal = input[wildcards.sample]["paternal_raw_data"] 
        maternal_path = f"results/yak/"+maternal+".yak"
        paternal_path = f"results/yak/"+paternal+".yak" 
        file.append(maternal_path)
        file.append(paternal_path)
    if mode == "hic":
        r1 = input[wildcards.sample]["r1"]
        r2 = input[wildcards.sample]["r2"]
        file.append(r1)
        file.append(r2)
    return file

def create_symlink_splits(wildcards):
    mode = input[(wildcards.sample)]["mode"]
    file = []
    if mode == "hic":
        r1 = basename + "/"+ input[wildcards.sample]["r1"]
        r2 = basename + "/"+ input[wildcards.sample]["r2"]
        file.append(r1)
        file.append(r2)
    return file
    


def get_mode_hap(wildcards):
    mode = input[(wildcards.sample)]["mode"]
    if mode == "trio":
        return [f"results/{wildcards.sample}/assembly/hifiasm/{wildcards.sample}.dip.hap1.p_ctg.gfa",f"results/{wildcards.sample}/assembly/hifiasm/{wildcards.sample}.dip.hap2.p_ctg.gfa"]
    elif mode == "hic":
        return [f"results/{wildcards.sample}/assembly/hifiasm/{wildcards.sample}.hic.hap1.p_ctg.gfa",f"results/{wildcards.sample}/assembly/hifiasm/{wildcards.sample}.hic.hap2.p_ctg.gfa"]
    else:
        return [f"results/{wildcards.sample}/assemblyhifiasm/{wildcards.sample}.bp.hap1.p_ctg.gfa",f"results/{wildcards.sample}/assembly/hifiasm/{wildcards.sample}.bp.hap2.p_ctg.gfa"]
 
def all_input(wildcards):
    wanted_input = []  
    wanted_input.extend(
        expand(
            [ 
                "results/{sample}/qc/"+assembler+"/{sample}_hap{haplotype}.katplot.png",
                "results/{sample}/telomeres/"+assembler+"/{sample}_hap{haplotype}_telomeric_repeat_windows.tsv",
                "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.fasta",
            ],
            sample = input,
            haplotype=haplotype
        )
    )
    wanted_input.extend(
        expand(
            [
                "results/{sample}/qc/{sample}_genomescope/linear_plot.png", 
                "results/{sample}/quast/"+assembler+"/report.html"
            ],
            sample = input,
        )
    ) 
    wanted_input.extend(
        expand(
            [
                "results/{sample}/busco/"+assembler+"/{sample}_hap{haplotype}/short_summary.specific.{lineage}.{sample}_hap{haplotype}.txt"
            ],
            sample = input,
            haplotype=haplotype,
            lineage=lineage
        )
    ) 
    for sample in input.keys(): 
        mode = input[sample]["mode"] 
        if mode == 'trio':
            wanted_input.extend(
                expand(
                    [
                        # "results/{sample}/merqury_trio/{sample}.hapmers.blob.png",
                        # "results/{sample}/quast/report.txt",
                        # "results/{sample}/merqury_trio/{sample}.{sample}_hap1.block.N.png",
                        # "results/{sample}/merqury_trio/{sample}.completeness.stats",
                        "results/{sample}/report_trio_"+assembler+".html"

                    ],
                    sample = sample
                )
            ) 
        else:
            wanted_input.extend(
                expand(
                    [ 
                        # "results/{sample}/merqury/{sample}.qv",
                        # "results/{sample}/merqury/{sample}.{sample}_hap1.spectra-cn.fl.png",
                        # "results/{sample}/merqury/{sample}.{sample}_hap2.spectra-cn.fl.png",
                        # "results/{sample}/merqury/{sample}.completeness.stats",
                        # "results/{sample}/merqury/{sample}.{sample}_hap1.qv",
                        # "results/{sample}/merqury/{sample}.{sample}_hap2.qv", 
                        "results/{sample}/scaffolding/"+assembler+"/{sample}_hap1/scaffolds_FINAL.fasta",
                        "results/{sample}/scaffolding/"+assembler+"/{sample}_hap2/scaffolds_FINAL.fasta",
                        "results/{sample}/report_default_"+assembler+".html"
                    ],
                    sample = sample
                )
            )
    return wanted_input

# onsuccess:
#      from snakemake.report import auto_report
#      auto_report(workflow.persistence.dag, "report/report.html")
    
    
rule all:
    input:
        all_input


# Counting k-mers with meryl and create histogram
rule meryl_count:
    input:
        lambda wildcards: input[wildcards.sample]["raw_data"]
    output:
        meryl = directory("results/meryl/{sample}.meryl"),
        histogram = "results/meryl/{sample}.histogram"
    threads: 
        config["threads"]["meryl"] 
    params:
        threads = config["threads"]["meryl"],
        kmer = config["kmer"]
    singularity:
        config["container"]["meryl"]
    shell:"""
        meryl k={params.kmer} count threads={params.threads} {input} output {output.meryl} ;
        meryl histogram {output.meryl} > {output.histogram}  
    """

# Copy data for trio mode
rule copy_trio:
    input:  
        unpack(create_symlink),
        child = rules.meryl_count.output.meryl
    output:
        maternal = directory("results/{sample}/merqury_trio/{sample}_maternal.meryl"),
        paternal = directory("results/{sample}/merqury_trio/{sample}_paternal.meryl"),
        child    = directory("results/{sample}/merqury_trio/{sample}.meryl")
    shell:"""
        cp -Rf {input[0]} {output.maternal}; 
        cp -Rf {input[1]} {output.paternal};
        cp -Rf {input.child} {output.child}
    """ 

# Copy data for default or hic mode
rule copy_default:
    input:   
        child = rules.meryl_count.output.meryl
    output:
        child    = directory("results/{sample}/merqury/"+assembler+"/{sample}.meryl")
    shell:"""
        cp -Rf {input.child} {output.child}
    """ 

# Only perform for trio binning mode
rule meryl_hapmers:
    input:
        maternal = rules.copy_trio.output.maternal,
        paternal = rules.copy_trio.output.paternal,
        child = rules.copy_trio.output.child
    output: 
        maternal = directory("results/{sample}/merqury_trio/{sample}_maternal.hapmer.meryl"),
        paternal = directory("results/{sample}/merqury_trio/{sample}_paternal.hapmer.meryl")
    singularity:
        config["container"]["merqury"]
    params: 
        directory = "results/{sample}/merqury_trio"
    shell:"""  
        mkdir -p {params.directory};
        cd {params.directory};
        export MERQURY=/usr/local/share/merqury;
        sh  $MERQURY/trio/hapmers.sh {input}
    """

#  count kmers to estimate genome size
rule jellyfish:
    input: 
        lambda wildcards: input[wildcards.sample]["raw_data"]
    output:
        jf = "results/{sample}/qc/{sample}.jf"
    threads: 
        config["threads"]["jellyfish"] 
    params:
        threads = config["threads"]["jellyfish"],
        kmer = config["kmer"]
    singularity:
        config["container"]["jellyfish"]
    shell:"""
        jellyfish count -m {params.kmer} -s 100M -t {params.threads} -o {output.jf} -C <(zcat {input});
    """

#  compute the histogram of the k-mer occurrences
rule jellyfish_histo:
    input:
        rules.jellyfish.output.jf
    output:
        histo = "results/{sample}/qc/{sample}_jf.histo"
    threads: 
        config["threads"]["jellyfish"] 
    params:
        threads = config["threads"]["jellyfish"]
    singularity:
        config["container"]["jellyfish"]
    shell:"""
        jellyfish histo -h 1000000 -t {params.threads} {input} > {output.histo}
    """

# Plot Profile with GenomeScope2
rule genomescope:
    input:
        rules.jellyfish_histo.output.histo
    output:
        directory = directory("results/{sample}/qc/{sample}_genomescope"), 
        png = "results/{sample}/qc/{sample}_genomescope/linear_plot.png"
    params:
        ploidy = config["ploidy"],
        kmer = config["kmer"]
    singularity:
        config["container"]["genomescope"]
    shell:"""
        genomescope2 -k {params.kmer} -i {input} -o {output.directory} -p {params.ploidy}
    """

 
# count k-mers with yak
rule yak:
    input:
        lambda wildcards: input[wildcards.sample]["raw_data"]
    output:
        "results/yak/{sample}.yak"
    threads:
        config["threads"]["yak"]
    params:
        threads = config["threads"]["yak"]
    singularity:
        config["container"]["yak"]
    shell:"""
        yak count -k31 -b37 -t {params.threads}  -o {output} {input}
    """


rule copy_hifiasm_trio:
    input:  
        unpack(create_symlink_hifiasm)
    output:
        maternal = "results/{sample}/assembly/hifiasm/yak/{sample}_maternal.yak",
        paternal = "results/{sample}/assembly/hifiasm/yak/{sample}_paternal.yak"
    shell:"""
        ln -nrfs {input[0]} {output.maternal}; 
        ln -nrfs {input[1]} {output.paternal}; 
    """ 

# haplotype-resolved assemblies with trio binning
rule hifiasm_trio:
    input: 
        maternal = rules.copy_hifiasm_trio.output.maternal,
        paternal = rules.copy_hifiasm_trio.output.paternal,
        child = lambda wildcards: input[wildcards.sample]["raw_data"] 
    output:
        haplotype1 = "results/{sample}/assembly/hifiasm/{sample}.dip.hap1.p_ctg.gfa",
        haplotype2 = "results/{sample}/assembly/hifiasm/{sample}.dip.hap2.p_ctg.gfa",
    params:
        prefix = "results/{sample}/assembly/hifiasm/{sample}",
        ul = with_ont
    log:
        "results/{sample}/assembly/hifiasm/{sample}.hifiasm_trio.out"
    threads:  
        config["threads"]["hifiasm"]
    singularity:
        config["container"]["hifiasm"]
    shell:"""
        hifiasm {params.ul} -o {params.prefix} -t {threads} -1 {input.maternal} -2 {input.paternal} {input.child} 2> {log}
    """

# haplotype-resolved assemblies with default mode
rule hifiasm:
    input: 
        lambda wildcards: input[wildcards.sample]["raw_data"]
    output:
        haplotype1 = "results/{sample}/assembly/hifiasm/{sample}.bp.hap1.p_ctg.gfa",
        haplotype2 = "results/{sample}/assembly/hifiasm/{sample}.bp.hap2.p_ctg.gfa"
    params:
        prefix = "results/{sample}/assembly/hifiasm/{sample}",
        ul = with_ont
    threads: 
        config["threads"]["hifiasm"]
    log:
        "results/{sample}/assembly/hifiasm/{sample}.hifiasm.out"
    singularity:
        config["container"]["hifiasm"] 
    shell:"""
        hifiasm {params.ul} -t {threads} -l3 -o {params.prefix} -t {params.threads} {input} 2> {log}
    """
    
# haplotype-resolved assemblies with HiC
rule hifiasm_hic:
    input:
        unpack(create_symlink_hifiasm), 
        hifi = lambda wildcards: input[wildcards.sample]["raw_data"]

    output:
        haplotype1 = "results/{sample}/assembly/hifiasm/{sample}.hic.hap1.p_ctg.gfa",
        haplotype2 = "results/{sample}/assembly/hifiasm/{sample}.hic.hap2.p_ctg.gfa"
    params:
        prefix = "results/{sample}/assembly/hifiasm/{sample}", 
        ul = with_ont
    threads: 
        config["threads"]["hifiasm"]
    log:
        "results/{sample}/assembly/hifiasm/{sample}.hifiasm_hic.out"
    singularity:
        config["container"]["hifiasm"] 
    shell:"""
        hifiasm {params.ul} -t {threads} -o {params.prefix}  --h1 {input[0]} --h2 {input[1]} {input.hifi} 2> {log}
    """

# Verkko with Hic
rule verkko_hic:
    input:
        unpack(create_symlink_hifiasm), 
        hifi = lambda wildcards: input[wildcards.sample]["raw_data"]

    output:
        haplotype1 = "results/{sample}/assembly/verkko/assembly.haplotype1.fasta",
        haplotype2 = "results/{sample}/assembly/verkko/assembly.haplotype2.fasta"
    params:
        directory = "results/{sample}/assembly/verkko/",
        ul = with_ont,
        #snake = "--snakeopts '--jobs 50 --cluster \"/lustre/drocg/genomeassembly_with_verkko/slurm-sge-submit.sh {threads} {resources.mem_gb} {resources.time_h} {rulename} {resources.job_id} -p agap_long --mem 100000 \"'"
    threads: 
        config["threads"]["verkko"]
    #singularity:
    #    config["container"]["verkko"] 
    shell:"""
        module load verkko/2.0-conda;
        verkko {params.ul} --threads {threads} --no-nano   -d {params.directory} --hic1 {input[0]} --hic2 {input[1]} --hifi {input.hifi}  --snakeopts '--jobs 50 --cluster \"/lustre/drocg/genomeassembly_with_verkko/slurm-sge-submit.sh {{threads}}  {{rulename}}  -p agap_long  \"'
    """
#--snakeopts '--jobs 50 --cluster "/lustre/drocg/genomeassembly_with_verkko/slurm-sge-submit.sh {threads} {resources.mem_mb} {rulename} -p agap_long --mem 100000 "'
    #"""

# Convert GFA to fasta
rule gfa2fa:
    input:
        hap = get_mode_hap
    output:
        hap1_fa = "results/{sample}/assembly/hifiasm/{sample}_hap1_hifiasm.fasta",
        hap2_fa = "results/{sample}/assembly/hifiasm/{sample}_hap2_hifiasm.fasta"
    singularity:
        config["container"]["gfatools"]
    shell:"""
        gfatools gfa2fa {input.hap[0]} > {output.hap1_fa};
        gfatools gfa2fa {input.hap[1]} > {output.hap2_fa}
    """

# Run minimap2 to align pacbio data and generate paf files
# Calculate read depth histogram and base-level read depth. 
rule purge_dups_cutoffs:
    input:
        fasta = "results/{sample}/assembly/hifiasm/{sample}_hap{haplotype}_hifiasm.fasta" if assembler == "hifiasm"  else "results/{sample}/assembly/verkko/assembly.haplotype{haplotype}.fasta" ,
        reads = lambda wildcards: input[wildcards.sample]["raw_data"]
    output:
        paf = "results/{sample}/purge_dups/{sample}_hap{haplotype}/{sample}_hap{haplotype}.paf.gz",
        calcuts = "results/{sample}/purge_dups/{sample}_hap{haplotype}/calcuts.log",
        cutoffs = "results/{sample}/purge_dups/{sample}_hap{haplotype}/cutoffs"
    params:
        directory="results/{sample}/purge_dups/{sample}_hap{haplotype}",
        threads  = config["threads"]["purge_dups"]
    threads: 
        config["threads"]["purge_dups"]
    singularity:
        config["container"]["purge_dups"]
    shell:"""
        minimap2  -t {params.threads} -x map-hifi {input.fasta} {input.reads} | gzip -c - > {output.paf};
        pbcstat {params.directory}/*.paf.gz -O {params.directory};
        calcuts {params.directory}/PB.stat > {output.cutoffs} 2>{output.calcuts}
    """

# Split an assembly and do a self-self alignment
# Purge haplotigs and overlaps
# Get purged primary and haplotig sequences from draft assembly
rule purge_dups:
    input:
        fasta = "results/{sample}/assembly/hifiasm/{sample}_hap{haplotype}_hifiasm.fasta" if assembler == "hifiasm"  else "results/{sample}/assembly/verkko/assembly.haplotype{haplotype}.fasta" ,
        cutoffs = rules.purge_dups_cutoffs.output.cutoffs
    output:
        purge = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/{sample}_hap{haplotype}.purged.fa",
        split = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/{sample}_hap{haplotype}.split",
        self_paf = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/{sample}_hap{haplotype}.split.self.paf.gz",
        bed = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/dups.bed",
        log = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/purge_dups.log",
        final = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}_purge_dups.fasta"
    params:
        directory="results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}",
        threads  = config["threads"]["purge_dups"]
    threads: 
        config["threads"]["purge_dups"]
    singularity:
        config["container"]["purge_dups"]
    shell:"""
        split_fa {input.fasta} > {output.split};
        minimap2 -x map-hifi -t {params.threads} -DP {output.split} {output.split} | gzip -c - > {output.split}.self.paf.gz;
        purge_dups -2 -T cutoffs -c {params.directory}/PB.base.cov {output.self_paf} > {output.bed} 2> {output.log};
        get_seqs -e {output.bed} {input.fasta} -p {params.directory}/{wildcards.sample}_hap{wildcards.haplotype};
        cp {output.purge} {output.final}
    """


rule samtools_index:
    input:
        fasta = rules.purge_dups.output.final
    output:
        fai = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}_purge_dups.fasta.fai",
        txt = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}_purge_dups.txt"
    singularity:
        config["container"]["samtools"]
    shell:"""
        samtools faidx {input.fasta};
        cut -f1,2 {output.fai} > {output.txt}
    """
 

rule bwa_index:
    input:
        fasta = rules.purge_dups.output.final
    output:
        index = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}_purge_dups.fasta.amb"
    singularity:
        config["container"]["bwa"]
    shell:"""
        bwa index {input.fasta};
    """

rule bwa_mem:
    input:
        unpack(create_symlink_hifiasm), 
        fasta = rules.purge_dups.output.final,
        fai = rules.bwa_index.output.index
    output:
        sam = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}_purge_dups.sam"
    threads: 
        config["threads"]["hifiasm"]
    params:
        threads  = config["threads"]["hifiasm"]
    singularity:
        config["container"]["bwa"] 
    shell:"""
        bwa mem -5SP -T0 -t{params.threads} {input.fasta} {input[0]} {input[1]} -o {output.sam}
    """
# find ligation junctions in Omni-C (and other proximity ligation) libraries
rule pairtools_parse:
    input:
        unpack(create_symlink_hifiasm), 
        sam = rules.bwa_mem.output.sam,
        genome = rules.samtools_index.output.txt
    output:
        temp("results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}_purge_dups.mapped.pairsam")
    threads:
        config["threads"]["purge_dups"]
    singularity:
        config["container"]["pairtools"] 
    shell:"""
        pairtools parse --min-mapq 40 --walks-policy 5unique --max-inter-align-gap 30 --nproc-in {threads} --nproc-out {threads} --chroms-path {input.genome} {input.sam} > {output}
    """



rule pairtools_sort:
    input:
        rules.pairtools_parse.output
    output:
        temp("results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}_purge_dups.mapped.pairsam.sorted")
    threads:
        config["threads"]["purge_dups"]
    singularity:
        config["container"]["pairtools"] 
    params:
        directory=basename+"/results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}_tmp"
    shell:"""
        mkdir -p {params.directory};
        pairtools sort --tmpdir={params.directory}  --nproc {threads} {input} > {output} 
    """

#detects molecules that could be formed via PCR duplication and tags them as “DD” pair type.

rule pairtools_dedup:
    input:
        rules.pairtools_sort.output
    output: 
        stats = "results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}_purge_dups.stats.txt",
        dedup = "results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}_purge_dups.pairsam"
    threads:
        config["threads"]["purge_dups"]
    singularity:
        config["container"]["pairtools"] 
    shell:""" 
        pairtools dedup --nproc-in {threads} --nproc-out {threads} --mark-dups --output-stats {output.stats} --output {output.dedup} {input}
    """



rule pairtools_split:
    input:
        rules.pairtools_dedup.output.dedup
    output:
        pairs = "results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}_purge_dups.mapped.pairs",
        bam = "results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}_purge_dups.mapped.bam"
    threads:
        config["threads"]["purge_dups"]
    singularity:
        config["container"]["pairtools"] 
    shell:""" 
        pairtools split --nproc-in {threads} --nproc-out {threads} --output-pairs {output.pairs} --output-sam {output.bam} {input}
    """

rule bam2bed:
    input:
        rules.pairtools_split.output.bam
    output:
        temp = temp("results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}_temp.bed"),
        bed = "results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}.bed",
    singularity:
        config["container"]["bedtools"]
    threads:
        config["threads"]["purge_dups"]
    shell:"""
        bamToBed -i {input} > {output.temp};
        sort -k {threads} {output.temp} > {output.bed}
    """

rule salsa:
    input:
        fasta = rules.purge_dups.output.final,
        bed = rules.bam2bed.output.bed,
        index = rules.samtools_index.output.fai
    output:
        fasta = "results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}/scaffolds_FINAL.fasta",
        agp = "results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}/scaffolds_FINAL.agp"
    singularity:
        config["container"]["salsa"]
    params:
        length = config["salsa"]["length"],
        iteration = config["salsa"]["iteration"],
        directory = "results/{sample}/scaffolding/"+assembler+"/{sample}_hap{haplotype}"

    shell:"""
         run_pipeline.py -a {input.fasta} -l {input.index} -b {input.bed} -c {params.length} -i {params.iteration} -e DNASE -o {params.directory} 
    """ 
rule ragtag:
    input:
        fasta = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}_purge_dups.fasta",
        reference = config["reference"]
    output:
        fasta = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/ragtag.scaffold.fasta",
        agp = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/ragtag.scaffold.agp",
        fasta_rename = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}_ragtag.fasta"
    params:
        directory= "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}",
        threads  = config["threads"]["ragtag"]    
    threads:
        config["threads"]["ragtag"] 
    singularity:
        config["container"]["ragtag"]
    shell:"""
        ragtag.py scaffold -o {params.directory} -f 5000 --mm2-params '-x asm5 -t {params.threads}' {input.reference} {input.fasta};
        cp {output.fasta} {output.fasta_rename}
    """


rule copy_fasta:
    input:
        hap1 = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap1_ragtag.fasta",
        hap2 = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap2_ragtag.fasta"
    output:
        hap1 = "results/{sample}/merqury_trio/"+assembler+"/{sample}_hap1.fasta",
        hap2 = "results/{sample}/merqury_trio/"+assembler+"/{sample}_hap2.fasta",
    shell:"""
        cp {input.hap1} {output.hap1};
        cp {input.hap2} {output.hap2};

    """
 
rule copy_fasta_default:
    input:
        hap1 = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap1_ragtag.fasta",
        hap2 = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap2_ragtag.fasta"
    output:
        hap1 = "results/{sample}/merqury/"+assembler+"/{sample}_hap1.fasta",
        hap2 = "results/{sample}/merqury/"+assembler+"/{sample}_hap2.fasta"
    shell:"""
        cp {input.hap1} {output.hap1};
        cp {input.hap2} {output.hap2};

    """
 
rule rename_header:
    input:
        fasta = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/ragtag.scaffold.fasta",
        agp   = "results/{sample}/purge_dups/"+assembler+"/{sample}_hap{haplotype}/ragtag.scaffold.agp"
    output:
        fasta = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.fasta",
        agp   = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.agp",
        txt   = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.txt"
    singularity:
        config["container"]["perllib"]
    params:
        haplotype = "{haplotype}"
    shell:"""
        scripts/rename_header.pl {input.fasta} {input.agp} {params.haplotype} {output.fasta} {output.agp} {output.txt}
    """

#### Check assembly quality using merqury, busco and quast

rule kat:
    input:
        fasta = rules.rename_header.output.fasta,
        jellyfish = rules.jellyfish.output.jf
    output:
        katplot =  "results/{sample}/qc/"+assembler+"/{sample}_hap{haplotype}.katplot.png",
        json = "results/{sample}/qc/"+assembler+"/{sample}_hap{haplotype}.dist_analysis.json"
    params:
        prefix = "{sample}_hap{haplotype}",
        path   = "results/{sample}/qc/"+assembler+"/{sample}_hap{haplotype}",
        threads  = config["threads"]["kat"] 
    threads: 
        config["threads"]["kat"] 
    singularity:
        config["container"]["kat"]
    shell:"""
        kat comp -o {params.path} -t {params.threads} -m 21 --output_type png -v {input.jellyfish} {input.fasta};
        kat plot spectra-cn -x 200 -o {output.katplot} {params.path}-main.mx;
    """
 
rule merqury_default:
    input:
        meryl_db = rules.copy_default.output.child,
        fasta = rules.copy_fasta_default.output
    output:
        qv = "results/{sample}/merqury/"+assembler+"/{sample}.qv",
        spectra_cn1 = "results/{sample}/merqury/"+assembler+"/{sample}.{sample}_hap1.spectra-cn.fl.png", 
        spectra_cn2 =  "results/{sample}/merqury/"+assembler+"/{sample}.{sample}_hap2.spectra-cn.fl.png", 
        completeness = "results/{sample}/merqury/"+assembler+"/{sample}.completeness.stats",
        qv_hap1 = "results/{sample}/merqury/"+assembler+"/{sample}.{sample}_hap1.qv",
        qv_hap2 = "results/{sample}/merqury/"+assembler+"/{sample}.{sample}_hap2.qv"
    threads: 
        config["threads"]["merqury"]  
    params:
        threads = config["threads"]["merqury"] ,
        directory = "results/{sample}/merqury/"+assembler,
        prefix = "{sample}"
    singularity:
        config["container"]["merqury"]
    shell:"""
        mkdir -p {params.directory};
        cd {params.directory};
        export MERQURY=/usr/local/share/merqury;
        sh $MERQURY/merqury.sh {input} {params.prefix};
     
    """
rule merqury_trio:
    input: 
        child = rules.copy_trio.output.child,
        maternal_hapmers = rules.meryl_hapmers.output.maternal,
        paternal_hapmers = rules.meryl_hapmers.output.paternal,
        fasta = rules.copy_fasta.output
    output:
        qv = "results/{sample}/merqury_trio/"+assembler+"/{sample}.qv", 
        completeness = "results/{sample}/merqury_trio/"+assembler+"/{sample}.completeness.stats",
        qv_hap1 = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap1.qv",
        qv_hap2 = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap2.qv", 
        spectra_cn1 = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap1.spectra-cn.fl.png", 
        spectra_cn2 = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap2.spectra-cn.fl.png",
        hapmers_blob = "results/{sample}/merqury_trio/"+assembler+"/{sample}.hapmers.blob.png",
        block_hap1 = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap1.block.N.png", 
        block_hap2 = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap2.block.N.png" 
    threads: 
        config["threads"]["merqury"]  
    params:
        threads = config["threads"]["merqury"],
        directory = "results/{sample}/merqury_trio/"+assembler,
        prefix = "{sample}"
    singularity:
        config["container"]["merqury"]
    shell:""" 
        cd {params.directory};
        export MERQURY=/usr/local/share/merqury;
        sh $MERQURY/merqury.sh {input} {params.prefix};
     
    """


       
rule quast:
    input:
        raw = expand("results/{{sample}}/assembly/hifiasm/{{sample}}_hap{haplotype}_hifiasm.fasta", zip, haplotype=haplotype) if assembler == "hifiasm"  else expand("results/{{sample}}/assembly/verkko/assembly.haplotype{haplotype}.fasta", zip, haplotype=haplotype),
        #hifiasm = expand("results/{{sample}}/assembly/{{sample}}_hap{haplotype}_hifiasm.fasta", zip, haplotype=haplotype),
        purge_dup = expand("results/{{sample}}/purge_dups/"+assembler+"/{{sample}}_hap{haplotype}_purge_dups.fasta", zip, haplotype=haplotype),
        ragtag = expand("results/{{sample}}/purge_dups/"+assembler+"/{{sample}}_hap{haplotype}_ragtag.fasta", zip, haplotype=haplotype)
    output:
        txt = "results/{sample}/quast/"+assembler+"/report.txt",
        tsv = "results/{sample}/quast/"+assembler+"/report.tsv",  
        html = "results/{sample}/quast/"+assembler+"/report.html"
    threads: 
        config["threads"]["quast"] 
    params:
        directory = "results/{sample}/quast/"+assembler,
        labels = " --labels ' raw hap1, raw hap2, purge_dup hap1, purge_dup hap2, ragtag hap1, ragtag hap2 '",
        #labels = " --labels ' purge_dup hap1, purge_dup hap2, ragtag hap1, ragtag hap2 '",
        threads  = config["threads"]["quast"] 
    singularity:
        config["container"]["quast"]        
    shell:"""  
        quast.py -o {params.directory} --threads {params.threads} {params.labels} {input.raw}  {input.purge_dup} {input.ragtag} 
    """

 #{input.hifiasm}
rule busco:
    input:
        rules.rename_header.output.fasta
    output:
        txt = "results/{sample}/busco/"+assembler+"/{sample}_hap{haplotype}/short_summary.specific.{lineage}.{sample}_hap{haplotype}.txt",
        json = "results/{sample}/busco/"+assembler+"/{sample}_hap{haplotype}/short_summary.specific.{lineage}.{sample}_hap{haplotype}.json"

    threads:
        config["threads"]["busco"] 
    singularity:
        config["container"]["busco"]
    params:
        prefix = "results/{sample}/busco/"+assembler,
        sample="{sample}_hap{haplotype}",
        lineage=config["busco_lineage"], 
        threads=config["threads"]["busco"] 
    shell:"""        
        busco -f -i {input} -l {params.lineage} --out_path {params.prefix} -o {params.sample} -m genome -c {params.threads}
    """
rule busco_plot:
    input:
        expand("results/{{sample}}/busco/"+assembler+"/{{sample}}_hap{haplotype}/short_summary.specific."+lineage +".{{sample}}_hap{haplotype}.txt", zip, haplotype=haplotype)
    output:
        "results/{sample}/busco_plot/"+assembler+"/busco_figure.png"
    singularity:
        config["container"]["busco"]
    params:
        directory = "results/{sample}/busco_plot/"+assembler
    shell:"""   
        cp {input} {params.directory};     
        generate_plot.py -wd {params.directory}
    """

use rule samtools_index as samtools_index_final with:
    input:
        fasta = rules.rename_header.output.fasta
    output:
        fai = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.fasta.fai",
        txt = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.fasta.txt"

rule bedtools_makewindows:
    input:
        rules.samtools_index_final.output.fai
    output:
        "results/{sample}/qc/"+assembler+"/{sample}_hap{haplotype}_final.bed"
    params:
        windows = config["windows"]
    singularity:
        config["container"]["bedtools"]
    shell:"""
        bedtools makewindows -g {input} -w {params.windows} > {output}
    """
    


# telomeres
rule find_telomeres:
    input:
        rules.rename_header.output.fasta
    output:
        tsv = "results/{sample}/telomeres/"+assembler+"/{sample}_hap{haplotype}_telomeric_repeat_windows.tsv",
        coverage = "results/{sample}/telomeres/"+assembler+"/{sample}_hap{haplotype}.txt"
    params:
        string = config["telomeric_motif"],
        directory = "results/{sample}/telomeres/"+assembler,
        prefix = "{sample}_hap{haplotype}"
    singularity:
        config["container"]["tidk"]
    shell:"""
        tidk search --string {params.string} --output {params.prefix} --dir {params.directory} {input};
        sed '1d' {output.tsv} |awk '{{print $1,(int(($2-1)/10000))*10000+1,$2,$3+$4}}' | grep chr > {output.coverage}
    """

rule draw_ideogram:
    input:
        karyotype = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.txt",
        telomere = "results/{sample}/telomeres/"+assembler+"/{sample}_hap{haplotype}.txt"
    output:
        svg = "results/{sample}/telomeres/"+assembler+"/{sample}_hap{haplotype}.svg",
        png = "results/{sample}/telomeres/"+assembler+"/{sample}_hap{haplotype}.png"
    singularity:
        config["container"]["rideogram"]
    shell:"""
        Rscript --vanilla scripts/plot_chromosome.R
    """

rule minimap2:
    input:
        reads = lambda wildcards: input[wildcards.sample]["raw_data"], 
        fasta = rules.rename_header.output.fasta 
    output:
        sam = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.sam"
    threads: 
        config["threads"]["ragtag"]
    params:
        threads  = config["threads"]["ragtag"]
    singularity:
        config["container"]["purge_dups"] 
    shell:"""
        minimap2 -t {params.threads} -ax map-hifi {input.fasta} {input.reads} > {output}
    """
rule samtools_coverage:
    input:
        sam = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.sam"
    output: 
        sorted_bam = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.sorted.bam",
        sorted_bai = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.sorted.bai",
        coverage = "results/{sample}/qc/"+assembler+"/{sample}_hap{haplotype}_final.coverage" 
    threads: 
        config["threads"]["ragtag"]
    params:
        threads  = config["threads"]["ragtag"]
    singularity:
        config["container"]["samtools"] 
    shell:"""
        samtools view -@ {threads} -S -b {input.sam} | samtools sort -@ {threads} -o {output.sorted_bam} -;
        samtools index {output.sorted_bam}  {output.sorted_bai};
        samtools coverage  -o {output.coverage} {output.sorted_bam};
    """

rule samtools_death:
    input:
        sam = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.sam",
        sorted_bam = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.sorted.bam",
        sorted_bai = "results/{sample}/final/"+assembler+"/{sample}_hap{haplotype}_final.sorted.bai",
        windows = "results/{sample}/qc/"+assembler+"/{sample}_hap{haplotype}_final.bed"
    output: 
        depth = "results/{sample}/qc/"+assembler+"/{sample}_hap{haplotype}_final.depth" 
    threads: 
        config["threads"]["ragtag"]
    params:
        threads  = config["threads"]["ragtag"]
    singularity:
        config["container"]["samtools"] 
    shell:"""
        samtools depth -@ {threads} -b {input.windows} -o {output.depth} {input.sorted_bam};
    """
 
 

rule report:
    input:
        # reads QC
        genomescope = "results/{sample}/qc/{sample}_genomescope/linear_plot.png",
        kaplot_haplotype1 = "results/{sample}/qc/"+assembler+"/{sample}_hap1.katplot.png",
        kaplot_haplotype2 = "results/{sample}/qc/"+assembler+"/{sample}_hap2.katplot.png",
        busco_haplotype1 = "results/{sample}/busco/"+assembler+"/{sample}_hap1/short_summary.specific."+lineage +".{sample}_hap1.txt",
        busco_haplotype2 = "results/{sample}/busco/"+assembler+"/{sample}_hap2/short_summary.specific."+lineage+".{sample}_hap2.txt",
        busco_plot = "results/{sample}/busco_plot/"+assembler+"/busco_figure.png",
        quast            = "results/{sample}/quast/"+assembler+"/report.tsv",
        spectra_cn1      = "results/{sample}/merqury/"+assembler+"/{sample}.{sample}_hap1.spectra-cn.fl.png",
        spectra_cn2      = "results/{sample}/merqury/"+assembler+"/{sample}.{sample}_hap2.spectra-cn.fl.png",
        qv              = "results/{sample}/merqury/"+assembler+"/{sample}.qv",
        scaffolding1    = "results/{sample}/final/"+assembler+"/{sample}_hap1_final.txt",
        scaffolding2    = "results/{sample}/final/"+assembler+"/{sample}_hap2_final.txt",
        qv_hap1         = "results/{sample}/merqury/"+assembler+"/{sample}.{sample}_hap1.qv",
        qv_hap2         = "results/{sample}/merqury/"+assembler+"/{sample}.{sample}_hap2.qv",
        completeness    = "results/{sample}/merqury/"+assembler+"/{sample}.completeness.stats",
        coverage1       = "results/{sample}/qc/"+assembler+"/{sample}_hap1_final.coverage" ,
        coverage2       = "results/{sample}/qc/"+assembler+"/{sample}_hap2_final.coverage" 
    output:
        "results/{sample}/report_default_"+assembler+".html"
    params:
        sample="{sample}",
        mode=get_mode,
        method=assembler
    singularity:
        config["container"]["markdown"]
    script:"./scripts/report.Rmd"


rule report_trio:
    input:
        # reads QC
        genomescope = "results/{sample}/qc/{sample}_genomescope/linear_plot.png",
        kaplot_haplotype1 = "results/{sample}/qc/"+assembler+"/{sample}_hap1.katplot.png",
        kaplot_haplotype2 = "results/{sample}/qc/"+assembler+"/{sample}_hap2.katplot.png",
        busco_haplotype1 = "results/{sample}/busco/"+assembler+"/{sample}_hap1/short_summary.specific."+lineage +".{sample}_hap1.txt",
        busco_haplotype2 = "results/{sample}/busco/"+assembler+"/{sample}_hap2/short_summary.specific."+lineage+".{sample}_hap2.txt",
        busco_plot = "results/{sample}/busco_plot/"+assembler+"/busco_figure.png",
        quast            = "results/{sample}/quast/"+assembler+"/report.tsv",
        spectra_cn1      = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap1.spectra-cn.fl.png",
        spectra_cn2      = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap2.spectra-cn.fl.png",
        qv              = "results/{sample}/merqury_trio/"+assembler+"/{sample}.qv",
        scaffolding1    = "results/{sample}/final/"+assembler+"/{sample}_hap1_final.txt",
        scaffolding2    = "results/{sample}/final/"+assembler+"/{sample}_hap2_final.txt",
        qv_hap1         = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap1.qv",
        qv_hap2         = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap2.qv",
        completeness    = "results/{sample}/merqury_trio/"+assembler+"/{sample}.completeness.stats",
        hapmers_blob    = "results/{sample}/merqury_trio/"+assembler+"/{sample}.hapmers.blob.png", 
        block_hap1      = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap1.block.N.png",
        block_hap2      = "results/{sample}/merqury_trio/"+assembler+"/{sample}.{sample}_hap2.block.N.png",
        coverage1       = "results/{sample}/qc/"+assembler+"/{sample}_hap1_final.coverage" ,
        coverage2       = "results/{sample}/qc/"+assembler+"/{sample}_hap2_final.coverage" 
    output:
        html = "results/{sample}/report_trio_"+assembler+".html",
        real_name = "results/{sample}/{sample}.html"

    params:
        sample="{sample}",
        mode=get_mode,
        method=assembler
    singularity:
        config["container"]["markdown"]
    script:
        "./scripts/report_trio.Rmd; cp {output.html} {output.real_name}"




