------------------------------------------------------------------------

merlot\_root
============

-   Sample : merlot\_root
-   Hifiasm mode : trio

------------------------------------------------------------------------

### Raw data QC

![](../images/trio/linear_plot.png)

### K-mer profiles

<table>
<thead>
<tr class="header">
<th>Hap 1</th>
<th>Hap 2</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><img src="../images/trio/merlot_root_hap1.katplot.png" /></td>
<td><img src="../images/trio/merlot_root_hap2.katplot.png" /></td>
</tr>
</tbody>
</table>

### k-mer completeness (recovery rate)

    merlot_root_hap1    all 326851224   413482556   79.0484
    merlot_root_hap2    all 327615159   413482556   79.2331
    both    all 407545237   413482556   98.5641
    merlot_root_hap1    merlot_root_maternal.hapmer 45687620    45762586    99.8362
    merlot_root_hap1    merlot_root_paternal.hapmer 3604    45098599    0.00799138
    merlot_root_hap2    merlot_root_maternal.hapmer 29167   45762586    0.0637355
    merlot_root_hap2    merlot_root_paternal.hapmer 44968247    45098599    99.711
    both    merlot_root_maternal.hapmer 45688173    45762586    99.8374
    both    merlot_root_paternal.hapmer 44968900    45098599    99.7124

#### Reference free QV estimation

    merlot_root_hap1    22789   520784823   56.8114 2.0838e-06
    merlot_root_hap2    3146    501222228   65.2449 2.98889e-07
    Both    25935   1022007051  59.1778 1.20842e-06

### Copy-number spectrum analysis

<table>
<thead>
<tr class="header">
<th>Hap 1</th>
<th>Hap 2</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><img src="../images/trio/merlot_root.merlot_root_hap1.spectra-cn.fl.png" /></td>
<td><img src="../images/trio/merlot_root.merlot_root_hap2.spectra-cn.fl.png" /></td>
</tr>
</tbody>
</table>

### Hap-mer blob plots

The blob plots provides a quick glance on the overall phasing across an
assembly. Each dot (circle) represents a sequence, a contig or scaffold,
with its size relative to the sequence length. Each sequence is colored
by assembly. The x and y axis are the number of hap-mers found.

![](../images/trio/merlot_root.hapmers.blob.png)

### Phased block statistics and switch error rates

Blocks are visualized in the N\* plots, ordered by its’ size along the
block sum coverage. The blocks are colored according to the haplotype.
The trio binned assemblies have small blocks of switches to the other
haplotype, which indicates the small fraction of reads being mis-binned.
(We expect this will become smaller when the error rate in the long
reads go down.)

<table>
<thead>
<tr class="header">
<th>Hap 1</th>
<th>Hap 2</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><img src="../images/trio/merlot_root.merlot_root_hap1.block.N.png" /></td>
<td><img src="../images/trio/merlot_root.merlot_root_hap2.block.N.png" /></td>
</tr>
</tbody>
</table>

### Quast report

    All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

    Assembly                     Haplotype1           Haplotype2         
    # contigs (>= 0 bp)          551                  121                
    # contigs (>= 1000 bp)       551                  121                
    # contigs (>= 5000 bp)       551                  121                
    # contigs (>= 10000 bp)      551                  121                
    # contigs (>= 25000 bp)      521                  111                
    # contigs (>= 50000 bp)      274                  74                 
    Total length (>= 0 bp)       520799563            501228848          
    Total length (>= 1000 bp)    520799563            501228848          
    Total length (>= 5000 bp)    520799563            501228848          
    Total length (>= 10000 bp)   520799563            501228848          
    Total length (>= 25000 bp)   520173890            501023771          
    Total length (>= 50000 bp)   510656719            499714254          
    # contigs                    551                  121                
    Largest contig               36558974             37716116           
    Total length                 520799563            501228848          
    Reference length             475604073            475604073          
    GC (%)                       36.21                35.23              
    Reference GC (%)             34.53                34.53              
    N50                          25072669             25445121           
    NG50                         25072669             25445121           
    N90                          20404945             20718820           
    NG90                         21213240             22641495           
    auN                          24389329.6           25978285.1         
    auNG                         26706987.9           27377952.9         
    L50                          9                    9                  
    LG50                         9                    9                  
    L90                          19                   18                 
    LG90                         17                   16                 
    # misassemblies              34618                32861              
    # misassembled contigs       104                  96                 
    Misassembled contigs length  491304317            497997314          
    # local misassemblies        13174                13440              
    # scaffold gap ext. mis.     9                    4                  
    # scaffold gap loc. mis.     4                    5                  
    # unaligned mis. contigs     4                    7                  
    # unaligned contigs          428 + 119 part       2 + 93 part        
    Unaligned length             103958516            78342029           
    Genome fraction (%)          77.768               79.318             
    Duplication ratio            1.124                1.118              
    # N's per 100 kbp            0.60                 0.70               
    # mismatches per 100 kbp     1225.86              1132.71            
    # indels per 100 kbp         196.38               184.30             
    # genomic features           512396 + 15740 part  521927 + 14847 part
    Largest alignment            1063399              1156563            
    Total aligned length         414200787            420008433          
    NA50                         23941                30799              
    NGA50                        29610                34903              
    NA90                         -                    -                  
    NGA90                        -                    -                  
    auNA                         63108.7              101559.1           
    auNGA                        69105.8              107031.0           
    LA50                         4263                 3082               
    LGA50                        3415                 2691               
    LA90                         -                    -                  
    LGA90                        -                    -                  

### BUSCO

#### Haplotype 1

    # BUSCO version is: 5.6.1 
    # The lineage dataset is: eudicots_odb10 (Creation date: 2024-01-08, number of genomes: 31, number of BUSCOs: 2326)
    # Summarized benchmarking in BUSCO notation for file /lustre/drocg/assembly/results/merlot_root/assembly/merlot_root_hap1/ragtag.scaffold.fasta
    # BUSCO was run in mode: euk_genome_met
    # Gene predictor used: metaeuk

        ***** Results: *****

        C:98.5%[S:96.3%,D:2.2%],F:0.8%,M:0.7%,n:2326       
        2291    Complete BUSCOs (C)            
        2240    Complete and single-copy BUSCOs (S)    
        51  Complete and duplicated BUSCOs (D)     
        18  Fragmented BUSCOs (F)              
        17  Missing BUSCOs (M)             
        2326    Total BUSCO groups searched        

    Assembly Statistics:
        551 Number of scaffolds
        582 Number of contigs
        520799563   Total length
        0.001%  Percent gaps
        25 MB   Scaffold N50
        14 MB   Contigs N50


    Dependencies and versions:
        hmmsearch: 3.1
        bbtools: 39.01
        metaeuk: 6.a5d39d9
        busco: 5.6.1

#### Haplotype 2

    # BUSCO version is: 5.6.1 
    # The lineage dataset is: eudicots_odb10 (Creation date: 2024-01-08, number of genomes: 31, number of BUSCOs: 2326)
    # Summarized benchmarking in BUSCO notation for file /lustre/drocg/assembly/results/merlot_root/assembly/merlot_root_hap2/ragtag.scaffold.fasta
    # BUSCO was run in mode: euk_genome_met
    # Gene predictor used: metaeuk

        ***** Results: *****

        C:98.1%[S:95.9%,D:2.2%],F:1.0%,M:0.9%,n:2326       
        2283    Complete BUSCOs (C)            
        2231    Complete and single-copy BUSCOs (S)    
        52  Complete and duplicated BUSCOs (D)     
        23  Fragmented BUSCOs (F)              
        20  Missing BUSCOs (M)             
        2326    Total BUSCO groups searched        

    Assembly Statistics:
        121 Number of scaffolds
        156 Number of contigs
        501228848   Total length
        0.001%  Percent gaps
        25 MB   Scaffold N50
        15 MB   Contigs N50


    Dependencies and versions:
        hmmsearch: 3.1
        bbtools: 39.01
        metaeuk: 6.a5d39d9
        busco: 5.6.1
