------------------------------------------------------------------------

cabernet\_franc
===============

-   Sample : cabernet\_franc
-   Hifiasm mode : default

------------------------------------------------------------------------

### Raw data QC

![](../images/default/linear_plot.png)

### K-mer profiles

<table>
<thead>
<tr class="header">
<th>Hap 1</th>
<th>Hap 2</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><img src="../images/default/cabernet_franc_hap1.katplot.png" /></td>
<td><img src="../images/default/cabernet_franc_hap2.katplot.png" /></td>
</tr>
</tbody>
</table>

### k-mer completeness (recovery rate)

Completeness

    cabernet_franc_hap1 all 324270084   420185750   77.173
    cabernet_franc_hap2 all 328121434   420185750   78.0896
    both    all 416282593   420185750   99.0711

#### Reference free QV estimation

    cabernet_franc_hap1 4595    499326983   63.5832 4.38211e-07
    cabernet_franc_hap2 3072    508798791   65.4134 2.87513e-07
    Both    7667    1008125774  64.4111 3.62154e-07

### Copy-number spectrum analysis

<table>
<thead>
<tr class="header">
<th>Hap 1</th>
<th>Hap 2</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><img src="../images/default/cabernet_franc.cabernet_franc_hap1.spectra-cn.fl.png" /></td>
<td><img src="../images/default/cabernet_franc.cabernet_franc_hap2.spectra-cn.fl.png" /></td>
</tr>
</tbody>
</table>

### Quast report

    All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

    Assembly                     Haplotype1           Haplotype2         
    # contigs (>= 0 bp)          274                  140                
    # contigs (>= 1000 bp)       274                  140                
    # contigs (>= 5000 bp)       274                  140                
    # contigs (>= 10000 bp)      274                  140                
    # contigs (>= 25000 bp)      234                  129                
    # contigs (>= 50000 bp)      81                   86                 
    Total length (>= 0 bp)       499335343            508804351          
    Total length (>= 1000 bp)    499335343            508804351          
    Total length (>= 5000 bp)    499335343            508804351          
    Total length (>= 10000 bp)   499335343            508804351          
    Total length (>= 25000 bp)   498491262            508566698          
    Total length (>= 50000 bp)   493486172            507017087          
    # contigs                    274                  140                
    Largest contig               32806999             46955701           
    Total length                 499335343            508804351          
    Reference length             475604073            475604073          
    GC (%)                       35.47                35.41              
    Reference GC (%)             34.53                34.53              
    N50                          25384715             24986928           
    NG50                         25384715             25956292           
    N90                          20726476             21664890           
    NG90                         22521784             22226361           
    auN                          25305902.3           26966948.2         
    auNG                         26568593.7           28849417.8         
    L50                          9                    9                  
    LG50                         9                    8                  
    L90                          18                   18                 
    LG90                         17                   16                 
    # misassemblies              36136                35737              
    # misassembled contigs       209                  133                
    Misassembled contigs length  497487810            508127781          
    # local misassemblies        13744                13958              
    # scaffold gap ext. mis.     2                    3                  
    # scaffold gap loc. mis.     1                    5                  
    # unaligned mis. contigs     4                    5                  
    # unaligned contigs          1 + 206 part         1 + 131 part       
    Unaligned length             81817856             82299096           
    Genome fraction (%)          76.401               78.089             
    Duplication ratio            1.146                1.145              
    # N's per 100 kbp            0.48                 0.45               
    # mismatches per 100 kbp     1245.20              1194.29            
    # indels per 100 kbp         199.92               191.81             
    # genomic features           503328 + 15939 part  513309 + 15126 part
    Largest alignment            1063856              1722562            
    Total aligned length         414751304            423543180          
    NA50                         25347                27637              
    NGA50                        28093                32318              
    NA90                         -                    -                  
    NGA90                        -                    -                  
    auNA                         69671.9              83514.9            
    auNGA                        73148.4              89344.8            
    LA50                         4052                 3591               
    LGA50                        3608                 3035               
    LA90                         -                    -                  
    LGA90                        -                    -                  

### BUSCO

#### Haplotype 1

    # BUSCO version is: 5.6.1 
    # The lineage dataset is: eudicots_odb10 (Creation date: 2024-01-08, number of genomes: 31, number of BUSCOs: 2326)
    # Summarized benchmarking in BUSCO notation for file /lustre/drocg/assembly/results/cabernet_franc/assembly/cabernet_franc_hap1/ragtag.scaffold.fasta
    # BUSCO was run in mode: euk_genome_met
    # Gene predictor used: metaeuk

        ***** Results: *****

        C:96.7%[S:94.2%,D:2.5%],F:0.8%,M:2.5%,n:2326       
        2249    Complete BUSCOs (C)            
        2191    Complete and single-copy BUSCOs (S)    
        58  Complete and duplicated BUSCOs (D)     
        18  Fragmented BUSCOs (F)              
        59  Missing BUSCOs (M)             
        2326    Total BUSCO groups searched        

    Assembly Statistics:
        274 Number of scaffolds
        298 Number of contigs
        499335343   Total length
        0.000%  Percent gaps
        25 MB   Scaffold N50
        23 MB   Contigs N50


    Dependencies and versions:
        hmmsearch: 3.1
        bbtools: 39.01
        metaeuk: 6.a5d39d9
        busco: 5.6.1

#### Haplotype 2

    # BUSCO version is: 5.6.1 
    # The lineage dataset is: eudicots_odb10 (Creation date: 2024-01-08, number of genomes: 31, number of BUSCOs: 2326)
    # Summarized benchmarking in BUSCO notation for file /lustre/drocg/assembly/results/cabernet_franc/assembly/cabernet_franc_hap2/ragtag.scaffold.fasta
    # BUSCO was run in mode: euk_genome_met
    # Gene predictor used: metaeuk

        ***** Results: *****

        C:98.2%[S:94.4%,D:3.8%],F:0.8%,M:1.0%,n:2326       
        2283    Complete BUSCOs (C)            
        2195    Complete and single-copy BUSCOs (S)    
        88  Complete and duplicated BUSCOs (D)     
        19  Fragmented BUSCOs (F)              
        24  Missing BUSCOs (M)             
        2326    Total BUSCO groups searched        

    Assembly Statistics:
        140 Number of scaffolds
        163 Number of contigs
        508804351   Total length
        0.000%  Percent gaps
        24 MB   Scaffold N50
        23 MB   Contigs N50


    Dependencies and versions:
        hmmsearch: 3.1
        bbtools: 39.01
        metaeuk: 6.a5d39d9
        busco: 5.6.1
